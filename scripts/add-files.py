#!/usr/bin/env python3

## Copyright (C) 2022 David Miguel Susano Pinto <pinto@robots.ox.ac.uk>
##
## Copying and distribution of this file, with or without modification,
## are permitted in any medium without royalty provided the copyright
## notice and this notice are preserved.  This file is offered as-is,
## without any warranty.

## Add new files to an existing LISA project.
##
## SYNOPSIS
##
##   add-file.py PROJECT-FILEPATH [FILE-SRC ...]
##
## DESCRIPTION
##
##   Adds the list of files and outputs a new project file.  It will
##   use the project `file_src_prefix' config value so only include
##   the file `src' you want to be used which is not necessarily the
##   path to the file to be added.  The files to be added need to
##   exist and be readable though, because the project file needs to
##   include image file attributes.
##
##   May be a good idea to confirm what was added after.  This can be
##   done with `diff' and `jq', like so:
##
##       ./add-files.py project.json [files ...] > project-plus.json
##       diff -u <(jq . project.json) <(jq . project-plus.json)


import argparse
import json
import os.path
import sys
import typing

import PIL.Image


Region = typing.Tuple[float, float, float, float]


class File(typing.TypedDict):
    fid: str
    src: str
    regions: typing.List[Region]
    fdata: typing.Dict
    rdata: typing.List[typing.Dict]


class Config(typing.TypedDict):
    navigation_from: int
    file_src_prefix: str
    item_height_in_pixel: int
    item_per_page: int
    show_attributes: typing.Dict
    navigation_to: int
    float_precision: int


class Project(typing.TypedDict):
    config: Config
    project: typing.Dict
    files: typing.List[File]
    attributes: typing.Dict


def create_file(fid: int, src_prefix: str, src: str) -> File:
    img = PIL.Image.open(os.path.join(src_prefix, src))
    return File(
        fid="%d" % fid,
        src=src,
        regions=[],
        fdata={"height": img.height, "width": img.width},
        rdata=[],
    )


def read_lisa_project(
    lisa_json_fpath: str,
) -> Project:
    with open(lisa_json_fpath, "r") as fh:
        lisa_json = json.load(fh)
    return lisa_json


def main(argv: typing.List[str]) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "project_filepath",
        help="LISA project file path",
    )
    parser.add_argument(
        "files_src",
        nargs="*",
        help="src value for all values",
    )
    args = parser.parse_args(argv[1:])

    lisa = read_lisa_project(args.project_filepath)

    # We only know about the height and width file attributes.
    assert set(lisa["attributes"]["file"].keys()) == {
        "height",
        "width",
    }, "unknown file attributes"

    # LISA does not actually require that the FID of the file to be
    # the position on the list.  But if we can't assume that they're
    # incrementing, then we don't know what FID to use on the files to
    # add.
    next_fid = int(lisa["files"][-1]["fid"]) +1
    assert len(lisa["files"]) == (
        next_fid -1
    ), "FID for last file is different from number of files"

    src_prefix = lisa["config"]["file_src_prefix"]
    for file_src in args.files_src:
        lisa["files"].append(create_file(next_fid, src_prefix, file_src))
        next_fid += 1

    print(json.dumps(lisa))
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
